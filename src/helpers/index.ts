import { IMessage } from '../interfaces';

export const formatTime = (date: string): string => {
  const formattedTime = new Date(date).toLocaleTimeString('uk-Uk', {
    hour: '2-digit',
    minute: '2-digit',
  });
  return formattedTime;
};

export const formatDateTime = (date: string): string => {
  const time = formatTime(date);
  const dateFormatted = new Date(date).toLocaleDateString('uk-UK');
  return `${dateFormatted} ${time}`;
};

export const formatMessageDate = (date: string): string => {
  return new Date(date).toLocaleDateString('en-EN', {
    month: 'long',
    day: 'numeric',
    year: 'numeric',
  });
};

export const isDateYesterday = (date: string): boolean => {
  let today = new Date();
  let yesterday = new Date();
  yesterday.setDate(today.getDate() - 1);
  return yesterday.toDateString() === new Date(date).toDateString();
};

export const isDateToday = (date: string): boolean => {
  const today = new Date();
  return today.toDateString() === new Date(date).toDateString();
};

export const groupMessagesByDate = (
  messages: IMessage[],
): { date: string; messages: IMessage[] }[] => {
  const groups = messages.reduce((groups: { [key: string]: IMessage[] }, message) => {
    const messageDate = message.createdAt;
    let date;
    if (isDateToday(messageDate)) date = 'Today';
    else if (isDateYesterday(messageDate)) date = 'Yesterday';
    else date = formatMessageDate(messageDate);
    if (!groups[date]) {
      groups[date] = [];
    }
    groups[date].push(message);
    return groups;
  }, {});

  const groupArrays = Object.keys(groups).map((date) => {
    return {
      date,
      messages: groups[date].sort(
        (a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      ),
    };
  });
  return groupArrays;
};
