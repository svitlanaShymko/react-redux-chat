import { IMessage } from "interfaces";
import * as actionTypes from "./actionTypes"

  
export const setMessages = (payload: IMessage[]) => ({
    type: actionTypes.SET_MESSAGES,
    payload
});

export const setPreloader = (payload: boolean) => ({
    type: actionTypes.SET_PRELOADER,
    payload
});

export const setEditModal = (payload: boolean) => ({
    type: actionTypes.SET_EDIT_MODAL,
    payload
});

export const sendMessage = (payload: IMessage) => ({
    type: actionTypes.SEND_MESSAGE,
    payload
});
  
export const editMessage = (payload: {id: string, newText: string}) => ({
    type: actionTypes.EDIT_MESSAGE,
    payload
});

export const deleteMessage = (payload: string) => ({
    type: actionTypes.DELETE_MESSAGE,
    payload
});

export const toggleLikeMessage = (payload: string)=>({
    type: actionTypes.TOGGLE_LIKE_MESSAGE,
    payload
})