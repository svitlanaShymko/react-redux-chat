import * as actionTypes from "./actionTypes"
import { IAction, IStoreState } from "./types";

const initialState: IStoreState = {
    chat: {
        messages: [],
        editModal: false,
        preloader: false,
    }
}

const rootReducer = (state:IStoreState = initialState, action: IAction)=>{
    const { type, payload } = action;
    switch(type){
        case actionTypes.SET_MESSAGES:{
            const {editModal, preloader} = state.chat;
            return {chat: {messages: payload, editModal, preloader}};
        }
        case actionTypes.SET_PRELOADER:{
            const { editModal, messages } = state.chat;
            return {chat: {preloader: payload, editModal, messages}};
        }
        case actionTypes.SET_EDIT_MODAL:{
            const { messages, preloader } = state.chat;
            return {chat: {preloader, editModal: payload, messages}};
        }
        case actionTypes.SEND_MESSAGE:{
            const {editModal, preloader} = state.chat;
            const updatedMessages = [...state.chat.messages, payload];
            return {chat: {messages: updatedMessages, editModal, preloader}};
        }
        case actionTypes.EDIT_MESSAGE:{
            const {editModal, preloader} = state.chat;
            let updatedMessages = [...state.chat.messages];
            const indexToUpdate = state.chat.messages.findIndex((message) => message.id === payload.id);
            updatedMessages[indexToUpdate] = {
            ...updatedMessages[indexToUpdate],
            text: payload.newText,
            editedAt: new Date().toISOString.toString(),
            };
            return {chat: {messages: updatedMessages, editModal, preloader}};
        }
        case actionTypes.DELETE_MESSAGE:{
            const {editModal, preloader} = state.chat;
            const updatedMessages = state.chat.messages.filter((message) => message.id !== payload);
            return {chat: {messages: updatedMessages, editModal, preloader}};
        }

        case actionTypes.TOGGLE_LIKE_MESSAGE: {
            const {editModal, preloader} = state.chat;
            let updatedMessages = [...state.chat.messages];
            const indexToUpdate = state.chat.messages.findIndex((message) => message.id === payload);
            updatedMessages[indexToUpdate] = {
            ...updatedMessages[indexToUpdate],
            isLiked: !updatedMessages[indexToUpdate]?.isLiked,
            };
            return {chat: {messages: updatedMessages, editModal, preloader}};
        }
        default:
            return state;
    }
}

export default rootReducer;