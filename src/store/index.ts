import { createStore, Dispatch, Store } from "redux"
import rootReducer from "./reducer"
import { IStoreState, IAction } from "./types"

export const store: Store<IStoreState, IAction> & {
  dispatch: Dispatch
} = createStore(rootReducer as any);