import { IMessage } from "interfaces"

export type IStoreState = {
    chat: {
        messages: IMessage[];
        editModal: boolean;
        preloader:boolean;
    }
}

export type IAction = {
    type: string;
    payload: any;
}