import React from 'react';
import Chat from './modules/Chat/Chat';

const App = ()=> {
  return (
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json"/>
  );
}

export default App;