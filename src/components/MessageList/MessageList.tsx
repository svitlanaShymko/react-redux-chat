import React from 'react';
import { CURRENT_USER } from '../../constants';
import { groupMessagesByDate } from '../../helpers';
import { IMessage } from '../../interfaces';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';
import './MessageList.css';

type Props = {
  messages: IMessage[];
  deleteMessage: (id: string) => void;
  openMessageModal: (id: string) => void;
  toggleLikeMessage: (id: string) => void;
};

const MessageList = ({ messages, deleteMessage, openMessageModal, toggleLikeMessage }: Props) => {
  return (
    <div className="message-list">
      {groupMessagesByDate(messages).map((messagesByDate, id) => (
        <div className="date-container" key={id}>
          <div className="messages-divider">{messagesByDate.date}</div>
          {messagesByDate.messages.map((message: IMessage, index:number) =>
            message.userId === CURRENT_USER.userId ? (
              <OwnMessage
                key={index}
                message={message}
                deleteMessage={deleteMessage}
                openMessageModal={openMessageModal}
              />
            ) : (
              <Message key={index} message={message} toggleLikeMessage={toggleLikeMessage} />
            ),
          )}
        </div>
      ))}
    </div>
  );
};

export default MessageList;
