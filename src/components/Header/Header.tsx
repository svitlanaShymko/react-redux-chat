import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComment, faUser } from '@fortawesome/free-solid-svg-icons';
import './Header.css';
import { formatDateTime } from '../../helpers';

type Props = {
  usersCount: number;
  messageCount: number;
  lastMessageDate: string;
};

const Header = ({ usersCount, messageCount, lastMessageDate }: Props) => {
  return (
    <header className="header">
      <div className="header-title">Group Chat</div>
      <div className="header-users-count">
        <FontAwesomeIcon icon={faUser} size="lg"/>
        <sup>{usersCount}</sup>
      </div>
      <div className="header-messages-count">
        <FontAwesomeIcon icon={faComment} size="lg"/>
        <sup>{messageCount}</sup>
      </div>
      <div className="header-last-message-date">{formatDateTime(lastMessageDate)}</div>
    </header>
  );
};

export default Header;
