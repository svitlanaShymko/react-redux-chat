import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { CURRENT_USER } from '../../constants';
import { IMessage } from '../../interfaces';
import Preloader from '../../components/Preloader/Preloader';
import Header from '../../components/Header/Header';
import MessageList from '../../components/MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import { useDispatch, useSelector } from "react-redux"
import { Dispatch } from 'redux';
import { deleteMessage, editMessage, sendMessage, setEditModal, setMessages, setPreloader, toggleLikeMessage } from '../../store/actionCreators';
import { IStoreState } from 'store/types';
import './Chat.css';
import EditMessageModal from '../../components/EditMessageModal/EditMessageModal';

type Props = {
  url: string;
};

const Chat = ({ url }: Props) => {
  const messages = useSelector((state: IStoreState) => state.chat.messages);
  const isLoading = useSelector((state: IStoreState) => state.chat.preloader);
  const isModalOpen = useSelector((state: IStoreState) => state.chat.editModal);

  const dispatch: Dispatch<any> = useDispatch()

  const [messageIdToEdit, setMessageIdToEdit] = useState('');

  const fetchMessages = useCallback(async (url: string): Promise<void> => {
    try {
      dispatch(setPreloader(true));
      const result = await fetch(url);
      const body = await result.json();
      dispatch(setMessages(body));
    } catch (err) {
      console.log(err);
    } finally {
      dispatch(setPreloader(false));
    }
  }, [dispatch]);

  useEffect(() => {
    fetchMessages(url);
  }, [url, fetchMessages]);


  const handleSendMessage = useCallback(
    (text: string) => {
    const newMessage: IMessage = {
      id: `${Math.random()}`,
      ...CURRENT_USER,
      text,
      createdAt: new Date().toISOString(),
      editedAt: '',
      avatar: ''
    };
    dispatch(sendMessage(newMessage));
  },
    [dispatch]
  );


  const handleDeleteMessage = useCallback(
    (id: string) => dispatch(deleteMessage(id)),
    [dispatch]
  )

  const handleToggleLikeMessage = useCallback(
    (id: string) => dispatch(toggleLikeMessage(id)),
    [dispatch]
  )
  const handleEditMessage = useCallback(
    (newText: string) => dispatch(editMessage({id:messageIdToEdit, newText})),
    [dispatch, messageIdToEdit]
  )

  const openMessageModal = useCallback(
    (id: string) => {
      dispatch(setEditModal(true));
      setMessageIdToEdit(id);
    },
    [dispatch]
  )

  const closeMessageModal = useCallback(
    () => {
      dispatch(setEditModal(false));
    },
    [dispatch]
  )

  const countUsers = (messages: IMessage[]) => {
    return new Set(messages.map((message) => message.user)).size; 
  };

  const getLatestMessageDate = useMemo(() => {
    const dateArr = messages.map(message=>message.createdAt).sort((a, b) => new Date(b).getTime() - new Date(a).getTime());
    return dateArr[0];
  }, [messages]);


  return isLoading ? (
    <Preloader />
  ) : (
    <div className="chat">
      <Header
        usersCount={countUsers(messages)}
        messageCount={messages.length}
        lastMessageDate={getLatestMessageDate}
      />
      <MessageList
        messages={messages}
        deleteMessage={handleDeleteMessage}
        openMessageModal={openMessageModal}
        toggleLikeMessage={handleToggleLikeMessage}
      />
      <MessageInput sendMessage={handleSendMessage} />
      {isModalOpen &&
      <EditMessageModal initialText={messages.find(message=>message.id===messageIdToEdit)?.text ?? ''} closeMessageModal={closeMessageModal} editMessage={handleEditMessage}/>
} 
    </div>
  );
};

export default Chat;
