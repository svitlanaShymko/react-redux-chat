import Chat from './src/modules/Chat/Chat'
import rootReducer from './src/store/reducer';

export default {
    Chat,
    rootReducer,
};